import Vue from 'vue';

import SnakesWebSocket from '../SnakesWebSocket.js';

const wsclient = new SnakesWebSocket('ws://' + SNAKES_HOST + ':6789');

wsclient.bind('open', (data) => {
    wsclient.send('new_client', 'guest');
})
wsclient.bind('tick', handleServerResponse);

let app = new Vue({
    el: '#leaderboard',
    data: {
        'players': [],
    }
})

function handleServerResponse(data) {
    const players = []
    const best_scores = data['best_scores']
    for (let i = 0; i < best_scores.length; i++) {
        players.push({
            'name': best_scores[i][0],
            'points': best_scores[i][1],
            'time': best_scores[i][2],
        })
    }
    app.players = players;
}

let form = document.getElementById("name-form");
form.addEventListener("submit", function(e) {
    e.preventDefault();
    let name = document.getElementById("name");
    sessionStorage.setItem("playername", name.value);
    window.location.href = "/game.html";
    return false;
})
