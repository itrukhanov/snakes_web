function dispatch(event_name, message, callbacks) {
    let chain = callbacks[event_name];
    if (typeof chain == 'undefined')
        return;
    for (let i = 0; i < chain.length; i++)
        chain[i](message);
}

function handleMessage(e, callbacks) {
    let json = JSON.parse(e['data']);
    dispatch(json['event'], json['data'], callbacks);
}

export default class SnakesWebSocket {
    constructor(url) {
        this.callbacks = {};
        this.conn = new WebSocket(url);
        this.conn.onopen = () => dispatch('open', null, this.callbacks);
        this.conn.onclose = () => dispatch('close', null, this.callbacks);
        this.conn.onmessage = (e) => handleMessage(e, this.callbacks);
    }

    bind(event_name, callback) {
        this.callbacks[event_name] = this.callbacks[event_name] || [];
        this.callbacks[event_name].push(callback);
        return this;
    }

    send(event_name, message) {
        let payload = JSON.stringify({
            'event': event_name,
            'data': message,
        })
        this.conn.send(payload);
        return this;
    }
}
