export default class Fruit {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.color = Math.floor(Math.random() * 16 ** 6);
    }

    isSameFruit(x, y) {
        return this.x == x && this.y == y;
    }
}
