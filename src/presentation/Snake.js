const PIXI = require('pixi.js');

export default class Snake {
    constructor(snake, playerID, appCenter, color) {
        this.playerID = playerID;
        this.color = color;
        this.container = new PIXI.Container();
        this.headContainer = new PIXI.Container();
        this.bodyContainer = new PIXI.Container();
        this.container.addChild(this.bodyContainer);
        this.container.addChild(this.headContainer);

        this.eyes = new PIXI.Graphics();
        this.eyes.beginFill(0xFFFFFF);
        this.eyes.drawCircle(0, 8, 8);
        this.eyes.drawCircle(0, -8, 8);
        this.eyes.endFill();
        this.eyes.beginFill(0x0);
        this.eyes.drawCircle(4, 7, 4);
        this.eyes.drawCircle(4, -7, 4);
        this.eyes.endFill();
        this.headContainer.addChild(this.eyes);
        this.headContainer.x = appCenter.x;
        this.headContainer.y = appCenter.y;

        this.head = snake[0];
    }

    updateBodyContainer(snake) {
        let snakeColor = this.color;
        for (let i = 0; i < snake.length; i++) {
            // So the head draws on top of everything else.
            let chunk = snake[snake.length - i - 1];

            if (i < this.bodyContainer.children.length) {
                this.bodyContainer.children[i].x = chunk.x;
                this.bodyContainer.children[i].y = chunk.y;
            }
            else {
                let circle = new PIXI.Graphics();
                circle.beginFill(snakeColor);
                circle.drawCircle(0, 0, 16);
                circle.endFill();
                circle.lineStyle(4, 0x1);
                circle.drawCircle(0, 0, 20);
                circle.endFill();
                circle.x = chunk.x;
                circle.y = chunk.y;
                this.bodyContainer.addChild(circle);
            }
        }
        this.head = snake[0];
    }

    updateHeadContainer(angle) {
        this.headContainer.rotation = angle;
        this.headContainer.x = this.bodyContainer.children[this.bodyContainer.children.length - 1].x;
        this.headContainer.y = this.bodyContainer.children[this.bodyContainer.children.length - 1].y;
    }

    getContainer() {
        return this.container;
    }
}
