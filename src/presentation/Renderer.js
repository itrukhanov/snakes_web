"use strict";

const PIXI = require('pixi.js');

import GameContainer from './GameContainer.js';

export default class Renderer {
    constructor(renderLoop, gameContainers) {
        let app = new PIXI.Application({
            'antialias': true,
        });

        document.body.appendChild(app.view);

        app.renderer.view.style.position = "absolute";
        app.renderer.view.style.display = "block";
        app.renderer.autoResize = true;
        app.renderer.resize(window.innerWidth, window.innerHeight);

        this.background = new PIXI.extras.TilingSprite(
            PIXI.Texture.fromImage('018.png'),
            app.renderer.width,
            app.renderer.height,
        );
        const backgroundContainer =  new PIXI.Container();
        backgroundContainer.addChild(this.background);

        this.mainContainer = new PIXI.Container();
        this.mainContainer.position.set(app.screen.width / 2, app.screen.height / 2);
        this.setupMainContainer(app);
        gameContainers.forEach((c) => this.mainContainer.addChild(c.getContainer()));
        this.gameContainers = gameContainers;

        const mainContainer = this.mainContainer;
        const updateContainers = () => this.updateContainers();
        PIXI.loader.load(function setup() {
            app.stage.addChild(backgroundContainer);
            app.stage.addChild(mainContainer);
            app.ticker.add(renderLoop);
            app.ticker.add(updateContainers);
        })
    }

    setupMainContainer(app) {
    }

    updateContainers() {
    }
}
