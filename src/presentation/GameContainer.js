const PIXI = require('pixi.js');

import Snake from './Snake.js';

export default class GameContainer {
    constructor(offset_x, offset_y) {
        this.offset_x = offset_x;
        this.offset_y = offset_y;

        this.container = new PIXI.Container();
        this.snakesContainer = new PIXI.Container();
        this.fruitsContainer = new PIXI.Container();
        this.container.addChild(this.snakesContainer);
        this.container.addChild(this.fruitsContainer);
        this.container.x = offset_x;
        this.container.y = offset_y;

        this.snakes = {};
    }

    updatePresentation(players, fruits) {
        this.updateSnakes(players);
        this.drawNewFruits(fruits);
        this.removeMissingFruits(fruits);
    }

    updateSnakes(players) {
        if (Object.keys(this.snakes).length > Object.keys(players).length)
            for (let playerID in this.snakes)
                if (!players.hasOwnProperty(playerID)) {
                    this.snakesContainer.removeChild(this.snakes[playerID].getContainer());
                    delete this.snakes[playerID];
                }

        const appCenter = {'x': window.innerWidth / 2, 'y': window.innerHeight / 2};
        for (let playerID in players) {
            if (!this.snakes.hasOwnProperty(playerID)) {
                if (players[playerID].color == undefined)
                    continue;
                this.snakes[playerID] = new Snake(players[playerID].snake, playerID, appCenter, players[playerID].color);
                this.snakesContainer.addChild(this.snakes[playerID].getContainer());
            }
            this.snakes[playerID].updateBodyContainer(players[playerID].snake);
            this.snakes[playerID].updateHeadContainer(players[playerID].direction, appCenter);
        }
    }

    drawNewFruits(fruits) {
        const fruitCircles = this.fruitsContainer.children;
        fruits = fruits.filter(fruit => {
            for (let i = 0; i < fruitCircles.length; i++)
                if (fruit.isSameFruit(fruitCircles[i].x, fruitCircles[i].y))
                    return false;
            return true;
        });
        for (let i = 0; i < fruits.length; i++) {
            const circle = new PIXI.Graphics();
            circle.beginFill(fruits[i].color);
            circle.drawCircle(0, 0, 12);
            circle.endFill();
            circle.x = fruits[i].x;
            circle.y = fruits[i].y;
            this.fruitsContainer.addChild(circle);
        }
    }

    removeMissingFruits(fruits) {
        const fruitCircles = this.fruitsContainer.children.filter(fruitCircle => {
            for (let i = 0; i < fruits.length; i++)
                if (fruits[i].isSameFruit(fruitCircle.x, fruitCircle.y))
                    return false;
            return true;
        })
        fruitCircles.forEach(fruitCircle => this.fruitsContainer.removeChild(fruitCircle));
    }

    getContainer() {
        return this.container;
    }
}
