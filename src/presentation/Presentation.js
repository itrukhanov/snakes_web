"use strict";

require('../main.css');

require("@babel/polyfill");
const Victor = require('victor');

import Fruit from './Fruit.js';
import SnakesWebSocket from '../SnakesWebSocket.js';
import GameContainer from '../presentation/GameContainer.js';

export default class Presentation {
    constructor(clientType, renderer) {
        this.wsclient = new SnakesWebSocket('ws://' + SNAKES_HOST + ':6789');
        this.wsclient.bind('open', (data) =>  {
            this.wsclient.send('new_client', clientType);
            this.handleOpen();
        });

        this.renderer = renderer;
        this.players = {};
        this.fruits = [];
        this.wsclient.bind('game_over', () => window.location.replace('lost.html'));
        this.wsclient.bind('field_limit', (data) => {
            this.handleFieldData(data);
        });
    }

    handleFieldData(data) {
        this.createGameContainers(data);
        this.renderer = new this.renderer(() => this.renderLoop(), this.gameContainers);
        this.wsclient.bind('tick', (data) => this.handleServerResponse(data));
    }

    handleOpen() {
    };

    renderLoop() {
        this.gameContainers.forEach((c) => c.updatePresentation(this.players, this.fruits));
    }

    createGameContainers(fieldLimit) {
        this.gameContainers = [new GameContainer(0, 0)];
    }

    handleServerResponse(data) {
        data.players.forEach(player => {
            if (this.players.hasOwnProperty(player.id))
                if (!this.players[player.id].hasOwnProperty('color'))
                    player.color = Math.floor((Math.random() * 16 ** 6));
                else
                    player.color = this.players[player.id].color;

            this.players[player.id] = player;
        })
        for (let playerID in this.players)
            if (!isPlayerStillPresent(playerID, data.players))
                delete this.players[playerID];
        // Add new fruits.
        const newFruits = data.fruits.filter(fruit => {
            for (let i = 0; i < this.fruits.length; i++)
                if (this.fruits[i].isSameFruit(fruit.x, fruit.y))
                    return false;
            return true;
        })
        newFruits.forEach(fruit => this.fruits.push(new Fruit(fruit.x, fruit.y)));
        // Remove missing fruits.
        const missingFruits = this.fruits.filter(fruit => {
            for (let i = 0; i < data.fruits.length; i++)
                if (fruit.isSameFruit(data.fruits[i].x, data.fruits[i].y))
                    return false;
            return true;
        });
        missingFruits.forEach(fruit => {
            this.fruits = this.fruits.filter(f => f != fruit);
        });
    }
}

function isPlayerStillPresent(playerID, players) {
    for (let i = 0; i < players.length; i++)
        if (playerID == players[i].id)
            return true;

    return false;
}
