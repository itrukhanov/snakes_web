import Vue from 'vue';

import Presentation from '../presentation/Presentation.js';
import AdminRenderer from './Renderer.js';

class Admin extends Presentation {
    constructor() {
        super('admin', AdminRenderer);
    }

    handleServerResponse(data) {
        super.handleServerResponse(data);
        handleServerResponse(data);
    }
}

let app = new Vue({
    el: '#online-players-count',
    data: {
        'count': 0,
    }
})

function handleServerResponse(data) {
    app.count = data.players.length;
}

new Admin()
