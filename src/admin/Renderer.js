import Renderer from '../presentation/Renderer.js';

export default class AdminRenderer extends Renderer {
    setupMainContainer(app) {
        this.mainContainer.scale.x = this.mainContainer.scale.y = 0.2;
    }
}
