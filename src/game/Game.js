import Vue from 'vue';

import Presentation from '../presentation/Presentation.js';
import GameContainer from '../presentation/GameContainer.js'
import GameRenderer from './Renderer.js';

class Game extends Presentation {
    constructor(playfieldLimit) {
        super('player', GameRenderer);
        this.playerName = sessionStorage.getItem('playername');
        if (!this.playerName)
            window.location.href = '/';
    }

    handleFieldData(data) {
        this.renderer = GameRenderer;
        super.handleFieldData(data);
        this.renderer.handleMouseMove = (d) => this.handleMouseMove(d);
    }

    createGameContainers(fieldLimit) {
        fieldLimit = fieldLimit * 2;
        this.gameContainers = [
            new GameContainer(-fieldLimit, -fieldLimit), // top left
            new GameContainer(0, -fieldLimit),
            new GameContainer(fieldLimit, -fieldLimit), // top right
            new GameContainer(-fieldLimit, 0),
            new GameContainer(0, 0),
            new GameContainer(fieldLimit, 0),
            new GameContainer(-fieldLimit, fieldLimit),
            new GameContainer(0, fieldLimit),
            new GameContainer(fieldLimit, fieldLimit),
        ];
    }

    handleMouseMove(direction) {
        this.wsclient.send('player_direction', {'direction': direction});
    }

    handleOpen() {
        this.wsclient.send('player_joined', this.playerName);
    }

    handleServerResponse(data) {
        super.handleServerResponse(data);
        this.renderer.updateContainers(this.players, data['player_id']);
        updatePointsView(data);
    }
}

let app = new Vue({
    el: '#overlays',
    data: {
        'player': 0,
        'players': [],
    },
})

function updatePointsView(players) {
    const mainPlayerID = players['player_id'];
    const mainPlayer = players['players'].filter((ply) => ply.id == mainPlayerID)[0];
    app.player = mainPlayer.points;
    app.players = players['players'];
}

new Game();
