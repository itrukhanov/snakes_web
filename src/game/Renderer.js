import Victor from 'victor';

import Renderer from '../presentation/Renderer.js';

export default class GameRenderer extends Renderer {
    constructor(clientType, gameContainers, handleMouseMove) {
        super(clientType, gameContainers);
        this.handleMouseMove = handleMouseMove;
    }

    setupMainContainer(app) {
        app.stage.interactive = true;
        app.stage.on('mousemove', (e) => this.handleMouseMove(calculateDirection(e)));
    }

    updateContainers(players, mainPlayerID) {
        if (players && players.hasOwnProperty(mainPlayerID)) {
            this.mainContainer.pivot.copy(players[mainPlayerID].snake[0]);
            this.background.tilePosition.x = -players[mainPlayerID].snake[0].x
            this.background.tilePosition.y = -players[mainPlayerID].snake[0].y
        }
    }
}

function calculateDirection(e) {
    let center_x = window.innerWidth / 2;
    let center_y = window.innerHeight / 2;
    let pointer_x = e.data.global.x;
    let pointer_y = e.data.global.y;

    let pc_vec_norm = new Victor(pointer_x - center_x, pointer_y - center_y).normalize();

    return pc_vec_norm.direction();
}
