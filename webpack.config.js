const path = require('path');
const webpack = require('webpack');
require("babel-register");

const config = {
    target: "web",
    entry: {
        'index': './src/index/main.js',
        'game': './src/game/Game.js',
        'admin': './src/admin/Admin.js',
    },
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: "[name].js",
    },
    devServer: {
      //publicPath: '/dist/',
      // The local filesystem directory where static html files
      // should be placed.
      // Put your main static html page containing the <script> tag
      // here to enjoy 'live-reloading'
      // E.g., if 'contentBase' is '../views', you can
      // put 'index.html' in '../views/main/index.html', and
      // it will be available at the url:
      //   https://localhost:9001/main/index.html
      contentBase: path.resolve(__dirname, "./dist/"),
      // 'Live-reloading' happens when you make changes to code
      // dependency pointed to by 'entry' parameter explained earlier.
      // To make live-reloading happen even when changes are made
      // to the static html pages in 'contentBase', add 
      // 'watchContentBase'
      watchContentBase: true,
      compress: true,
    },
    module: {
        rules: [
            {test: /\.js$/, exclude: /node_modules/, use: "babel-loader"},
            {test: /\.css$/, use: ['style-loader', 'css-loader']},
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'SNAKES_HOST': JSON.stringify(process.env.SNAKES_HOST || 'localhost')
        })
    ],
    resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
    }
  }
};

module.exports = config;
